$(document).mouseup(function (e) {
    var container = $(".modal");

    if (container.has(e.target).length === 0) {
        container.hide();
        $(".modal").css("display", "none");
        history.pushState(false, "", ".");
    }
});


function saveLocalStorage() {
    localStorage.setItem("name", $("#name").val());
    localStorage.setItem("email", $("#email").val());
    localStorage.setItem("message", $("#message").val());
    localStorage.setItem("policy", $("#policy").prop("checked"));
}

function loadLocalStorage() {
    if (localStorage.getItem("name") !== null)
        $("#name").val(localStorage.getItem("name"));
    if (localStorage.getItem("email") !== null)
        $("#email").val(localStorage.getItem("email"));
    if (localStorage.getItem("message") !== null)
        $("#message").val(localStorage.getItem("message"));
    if (localStorage.getItem("policy") !== null) {
        $("#policy").prop("checked", localStorage.getItem("policy") === "true");
        if ($("#policy").prop("checked"))
            $("#sendButton").removeAttr("disabled");
    }
}
function clear() {
    localStorage.clear()
    $("#name").val("");
    $("#email").val("");
    $("#message").val("");
    $("#policy").val(false);
}

$(document).ready(function() {
    loadLocalStorage();
    $("#openButton").click(function() {
        $(".modal").css("display","flex");
        history.pushState(true, "", "./form");
    });
    $("#closeButton").click(function() {
        $(".modal").css("display", "none");
        history.pushState(false, "", ".");
    });
    $("#form").submit(function(e) {
        e.preventDefault();
        $(".modal").css("display", "none");
        let data =  $(this).serialize();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "https://formcarry.com/s/8vdBkQYWXU",
            data: data,
            success: function(response){
                if(response.status == "success"){
                    alert("Спасибо за сообщение!");
                    clear();
                } else {
                    alert("Произошла ошибка: " + response.message);
                }
            }
        });
    });
    $("#policy").change(function() {
        if(this.checked)
            $("#sendButton").removeAttr("disabled");
        else
            $("#sendButton").attr("disabled", "");
    })
    $("#form").change(saveLocalStorage);

    window.onpopstate = function(event) {
        if (event.state)
            $(".modal").css("display", "flex");
        else
            $(".modal").css("display", "none");
    };
})
